<?php
declare(strict_types=1);

namespace CarogaNET\Razr\DependencyInjection;

use CarogaNET\Razr\Commands\CommandInterface;
use CarogaNET\Razr\Commands\DefaultCommand;
use CarogaNET\Razr\Exceptions\DependencyExceptions;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class ContainerTest extends TestCase
{
    use ProphecyTrait;

    protected Container $commandContainer;
    protected string $commandName = 'razr:test:command';
    protected string $commandClassReference = 'textual_representation_of_a_class';

    public function setUp(): void
    {
        $this->commandContainer = new Container();
    }

    public function tearDown(): void
    {
        unset($this->commandContainer);
        unset($this->command);
    }

    public function testSet()
    {
        $this->commandContainer->set('CarogaNET\Razr\Commands\DefaultCommand');
        $this->assertTrue($this->commandContainer->has(DefaultCommand::$name));
    }

    public function testContainerDoesNotHasRequestedCommand()
    {
        $this->assertFalse($this->commandContainer->has('razr:test-2:command'));
    }

    public function testGet()
    {
        $this->commandContainer->set('CarogaNET\Razr\Commands\DefaultCommand');
        $this->assertInstanceOf(CommandInterface::class, $this->commandContainer->get(DefaultCommand::$name));
    }

    public function testGetUnregisteredCommand()
    {
        self::expectException(DependencyExceptions::class);
        self::expectExceptionMessage('Command razr:test:command is not registered.');
        $this->commandContainer->get('razr:test:command');
    }
}
