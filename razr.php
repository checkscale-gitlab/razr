#!/usr/local/bin/php
<?php

declare(strict_types=1);

use CarogaNET\Razr\App;

include_once 'vendor/autoload.php';

$app = new App(php_sapi_name());
$app->autoLoadCommands(__DIR__ . '/commands');
$app->runCommand($argv);
