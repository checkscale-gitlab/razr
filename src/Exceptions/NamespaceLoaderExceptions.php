<?php

declare(strict_types=1);

namespace CarogaNET\Razr\Exceptions;

class NamespaceLoaderExceptions extends \Exception
{
    public static function FileDoesNotContainANamespace($file)
    {
        throw new self("File {$file} does not contain a valid namespace.");
    }
}
