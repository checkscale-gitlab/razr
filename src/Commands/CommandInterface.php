<?php
declare(strict_types=1);

namespace CarogaNET\Razr\Commands;

interface CommandInterface
{
    public function execute(array $args = []);
    public function __toString(): string;
}
