<?php
declare(strict_types=1);

namespace CarogaNET\Razr\Commands;

use League\CLImate\CLImate;

abstract class Command implements CommandInterface
{
    /**
     * Holds the handle for calling this command. This can be anything as long as it's unique, is without spaces or tabs
     * and holds some form of pattern to your own liking.
     *
     * E.g.: app:seg:command
     *
     * @var string
     */
    public static string $name = '';
    public static string $description = '';
    protected CLImate $climate;

    public function __construct()
    {
        $this->climate = new CLImate();
    }

    public function execute(array $args = [])
    {
        $this->climate->br();
        /** @noinspection PhpUndefinedVariableInspection */
        $this->climate->tab()->out('The comand <light_red>'.get_called_class()::$name.'</light_red> doesn\'t do anything.');
        $this->climate->tab()->out('Try implementing the <light_red>`execute()`</light_red> method.');
        $this->climate->br();

    }

    public function __toString(): string
    {
        return self::class;
    }

    // @todo implement help() function to automatically display help messages when calling the command handle with :help or something
}
